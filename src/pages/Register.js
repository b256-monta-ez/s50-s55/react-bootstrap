import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register () {

	// State hooks to store the values of our input fields

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const { user } = useContext(UserContext);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// useEffect() - whenever there's a change in our webpage.
	useEffect (() => {

		if ((firstName !== '' && lastName !== '' && email !== '' && mobileNumber !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNumber.length >= 11)) {

			setIsActive(true)
		} else {

			setIsActive(false)
		}
	})

	function registerUser(e) {
		e.preventDefault();

		// clear input fields
		setFirstName('');
		setLastName('');
		setMobileNumber('');
		setPassword1('');
		setPassword2('');
		fetch('http://localhost:4000/users/checkEmail')
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(email !== data.email) {
				setEmail('');
			} else {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			}
		})

		alert('Thank you for registering!');
	}

	// onChange - checks if there are any changes inside of the input fields.
	// value = {email} the value stored in the input field will come from the value inside the getter "email"
	// setEmail(e.target.value) - sets the value of the getter email to the value stored in the value ={email} inside the input field.
	return (
		// (user.email !== null) ?
			// <Navigate to="/courses" />
		// :

		<Form onSubmit={e => registerUser(e)}>

		<Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicCheckbox">
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicCheckbox">
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control type="tel" placeholder="Enter Mobile Number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicCheckbox">
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicCheckbox">
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} />
	      </Form.Group>
	      <Form.Group className="mb-3" controlId="formBasicCheckbox">
	        
	      </Form.Group>

	      {/*Ternary operator*/}
	      {/*
				if(isActive === true) {
					<Button variant="primary" type="submit" id="submitBtn"> Submit </Button>
				} else { 
					<Button variant="danger" type="submit" id="submitBtn" disabled> Submit </Button> 
				}
	      */} 
			{
				isActive ?
				    <Button variant="primary" type="submit" id="submitBtn"> Submit </Button>
				:
					<Button variant="danger" type="submit" id="submitBtn" disabled> Submit </Button>
		    }
    	</Form>
	)
}