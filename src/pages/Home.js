import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home () {

	const data = {
		title: "Zuitt Coding Boocamp",
		content: "Opportunities for everyone, everywhere",
		destination: "/courses",
		label: "Enroll now!"
	}

	return (
		<>
			<Banner data ={data} />
			<Highlights />
		</>
	)
}